import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { createMuiTheme } from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import orange from '@material-ui/core/colors/orange';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import ReactGA from 'react-ga';
import configs from './config';

const theme = createMuiTheme({
    palette: {
      primary: teal,
      secondary: orange,
    },  
    typography: {
        useNextVariants: true,
    }
});

ReactGA.initialize(configs.googleAnalyticsID);

ReactDOM.render(<MuiThemeProvider theme={theme}>
    <App />
</MuiThemeProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
