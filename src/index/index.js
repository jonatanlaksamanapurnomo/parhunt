import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { withStyles } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'
import Typography from '@material-ui/core/Typography';
import CardContent from '../Content/CardContent';
import { observer, inject } from 'mobx-react'
import ReactGA from 'react-ga';
import {  Route, Switch, Redirect} from "react-router-dom";
import GraphContent from '../Content/GraphContent';

const styles = {
    page_color: {
        backgroundColor: grey['200'],
        minHeight:'100vh',
        overflow: 'auto'
    },
    start_row: {
        marginTop: "2rem"
    },
    card_parent: {
        padding: '0.5rem'
    }
}
class Index extends Component {
    componentDidMount() {
        const { appstore } = this.props;
        appstore.loadMatakuliah();

        ReactGA.pageview(window.location.pathname + window.location.search);
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.page_color}>
                <Grid>
                    <Row className={classes.start_row}>
                        <Col sm={8}>
                            <Typography variant="h5">
                                Halo!
                            </Typography>
                            <Typography variant="body1" style={{marginBottom:15}}>
                                Proyek ini dibangun diatas data yang disediakan oleh <a href="https://github.com/ftisunpar/data">Open Data FTIS</a>.
                                Saya mendirikan proyek ini untuk membantu mencari mata kuliah berikutnya yang akan saya ambil di semester berikutnya.<br/>
                                <b>Proyek ini masih dalam pembangunan, tidak semua fungsionalitas berjalan sesuai dengan ekspetasi.</b>
                            </Typography>
                            <Typography variant="body1" style={{marginBottom:25}}>
                                Data ditampilkan apa adanya dari project <a href="https://github.com/ftisunpar/data">Open Data FTIS</a>. Kontak ke project
                                tersebut jika terdapat bug atau perubahan. Semua data yang di tampilkan di sini akan otomatis terupdate mengikuti perubahan
                                yang ada pada project tersebut.
                            </Typography>
                            <Typography variant="body1" style={{marginBottom:15}}>
                                Punya saran dan kritik untuk proyek ini? Monggo ke <a href="https://gitlab.com/chez14/parhunt/">Laman Gitlab</a> project ini.
                            </Typography>
                        </Col>
                        <Col sm={4}>

                            <Typography variant="overline">
                                Promotion:
                            </Typography>
                            <a href="https://www.domainesia.com/?aff=4462" target="_blank" rel="noopener noreferrer"><img src="https://goo.gl/wdBX8M" alt="hosting murah"/></a>
                            <Typography variant="body2">
                                Gunakan kode promo <b>chez14-20</b> untuk hosting dan <b>chez14-20vps</b> untuk VPS.
                            </Typography>

                        </Col>
                    </Row>

                    {/*content*/}
                    <Switch>
                        <Route exact path="/" component={CardContent}/>
                        <Route  path="/card" component={CardContent}/>
                        <Route  path="/graph" component={GraphContent}/>
                        <Redirect to="/"/>
                    </Switch>


                </Grid>
            </div>
        )
    }
}

export default inject("appstore")(withStyles(styles)(observer(Index)));
