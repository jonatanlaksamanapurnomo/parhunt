import React,{ Component } from 'react';
import { ArcherContainer, ArcherElement } from 'react-archer';
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'
import { When, Then, Else, If } from 'react-if'
import {withStyles} from "@material-ui/core";


const styles = {
    rootStyle: {
        display: 'flex', justifyContent: 'center'
    },
    rowStyle: {
        margin: '200px 0', display: 'flex', justifyContent: 'space-between'
    },
    boxStyle: {
        padding: '10px', border: '1px solid black',
    },

};
// const rootStyle = { display: 'flex', justifyContent: 'center' };
// const rowStyle = { margin: '200px 0', display: 'flex', justifyContent: 'space-between', };
// const boxStyle = { padding: '10px', border: '1px solid black', };

class Graph extends  Component{
    render() {
        const {  classes,data } = this.props;

        let totalPrasyarat = 0;
        if (data.prasyarat.tempuh) {
            totalPrasyarat += data.prasyarat.tempuh.length;
        }
        if (data.prasyarat.lulus) {
            totalPrasyarat += data.prasyarat.lulus.length;
        }
        if (data.prasyarat.bersamaan) {
            totalPrasyarat += data.prasyarat.bersamaan.length;
        }
        if (data.prasyarat.berlakuAngkatan) {
            totalPrasyarat++;
        }

        let isMKU = data.kode.startsWith("MKU");
        return(
            //make root
            <ArcherContainer strokeColor='red' >
                <When condition={totalPrasyarat===0}>
                    <div style={classes.rootStyle}>
                        <ArcherElement
                            id={data.kode}
                            relations={[{
                                targetId: 'element2',
                                targetAnchor: 'top',
                                sourceAnchor: 'bottom',
                            }]}>
                            <div style={classes.boxStyle}>Root</div>
                        </ArcherElement>
                    </div>

                </When>
            </ArcherContainer>

        );
    }
}
Graph.propTypes = {
    data: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
}

export default observer(withStyles(styles)(Graph));