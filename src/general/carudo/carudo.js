import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Chip from '@material-ui/core/Chip'
import Divider from '@material-ui/core/Divider'
import Tooltip from '@material-ui/core/Tooltip'
import Avatar from '@material-ui/core/Avatar'
import brown from '@material-ui/core/colors/brown'

import MemoryIcon from '@material-ui/icons/Memory'


import { observer } from 'mobx-react'

import { When, Then, Else, If } from 'react-if'

import MKModal from './mkmodal'
import SemesterChips from './SemesterChips'


const styles = {
  card: {
  },
  mkname: {
    minHeight: "6rem",
    marginBottom: 20, 
    marginTop: 10
  },
  growableContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  growable: {
    flexGrow: 1
  }
};

const InfoChips = withStyles({
  root: {
    margin: 5,
    overflow: "visible"
  },
  colorPrimary: {
    backgroundColor: brown['800'],
    color: "#fff"
  }
})(Chip);


class Carudo extends Component {
  render() {
    const { classes, data } = this.props;

    let totalPrasyarat = 0;

    if (data.prasyarat.tempuh) {
      totalPrasyarat += data.prasyarat.tempuh.length;
    }
    if (data.prasyarat.lulus) {
      totalPrasyarat += data.prasyarat.lulus.length;
    }
    if (data.prasyarat.bersamaan) {
      totalPrasyarat += data.prasyarat.bersamaan.length;
    }
    if (data.prasyarat.berlakuAngkatan) {
      totalPrasyarat++;
    }

    let isMKU = data.kode.startsWith("MKU");
    return (
      <Card>
        <CardContent className={classes.card}>
          <div className={classes.growableContainer}>
            <Typography variant="subtitle1" className={classes.growable}>
              {data.kode}
            </Typography>
            <When condition={isMKU}>
              <Tooltip title={"Ini adalah Mata Kuliah Umum."}>
                <Chip variant="outlined" label="mku" style={{marginRight:5}}/>
              </Tooltip>
            </When>
            <When condition={data.wajib}>
              <Tooltip title={"Ini adalah mata kuliah yang wajib untuk diambil."}>
                <Chip color="primary" variant="outlined" icon={<MemoryIcon />} label="Wajib" />
              </Tooltip>
            </When>
          </div>

          <Typography variant="h6" className={classes.mkname}>
            {data.nama}
          </Typography>

          <div>
            <Tooltip title={"Mata kuliah biasanya diambil pada semester " + data.semester + "."}>
              <SemesterChips semester={data.semester}/>
            </Tooltip>
            <Tooltip title={"Mata kuliah ini menggunakan " + data.sks + " SKS (Satuan Kredit Semester)"}>
              <InfoChips variant="outlined" label="sks" avatar={<Avatar>{data.sks}</Avatar>} />
            </Tooltip>
          </div>
        </CardContent>
        <Divider />
        <CardActions>
          <Button variant="text" color="primary"  disabled={totalPrasyarat === 0} onClick={()=>{
            data.showDetail = true;
          }}>
            <If condition={totalPrasyarat > 0}>
              <Then> Prasyarat ({totalPrasyarat}) </Then>
              <Else> Tidak ada prasyarat </Else>
            </If>
          </Button>
        </CardActions>
        <MKModal data={data}/>
      </Card>
    )
  }
};

Carudo.propTypes = {
  data: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
}

export default observer(withStyles(styles)(Carudo));
