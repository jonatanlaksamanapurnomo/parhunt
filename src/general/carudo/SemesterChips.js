import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Chip from '@material-ui/core/Chip'
import { withStyles } from '@material-ui/core'

import { grey, brown, lightBlue, red, amber, green, lime, orange } from '@material-ui/core/colors'


export default class SemesterChips extends Component {
    static propTypes = {
        semester: PropTypes.number,
    }
    colorDark = grey['900']
    colorLight = grey['50']
    
    /**
     * Semester color, this color is generated from my
     * synesthesia. If you find this confusing, please make an issue,
     * and i'll manage it out hahahah.
     */
    semesterColor = [
        {   // semester 1
            backgroundColor: grey['300'],
            color: this.colorDark
        },
        {  // semester 2
            backgroundColor: brown['500'],
            color: this.colorLight
        },
        {  // semester 3
            backgroundColor: lightBlue['300'],
            color: this.colorDark
        },
        {  // semester 4
            backgroundColor: red['900'],
            color: this.colorLight
        },
        {  // semester 5
            backgroundColor: amber['500'],
            color: this.colorDark
        },
        {  // semester 6
            backgroundColor: green['800'],
            color: this.colorLight
        },
        {  // semester 7
            backgroundColor: lime['900'],
            color: this.colorLight
        },
        {  // semester 8
            backgroundColor: orange['500'],
            color: this.colorDark
        },
    ]
    


    render() {
        let { semester, variant } = this.props;
        
        if(semester === undefined || semester > 8 || semester < 1) {
            semester = 1;
        }
        semester--;
        
        let style = {
            root: {
                ...this.semesterColor[semester],
                margin: 5
            },
            outlined: {
                borderColor: this.semesterColor[semester].backgroundColor,
                color: this.semesterColor[semester].backgroundColor,
                backgroundColor: 'transparent'
            }
        }

        let Kambing = withStyles(style)(Chip);
        return (
            <Kambing label={"Semester " + (semester+1)} variant={variant} />
        )
    }
}
