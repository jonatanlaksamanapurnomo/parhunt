import React, { Component } from 'react'
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import { withStyles } from '@material-ui/core/styles';

import { Grid, Row, Col } from 'react-flexbox-grid';

import gitlab_logo from '../e_assets/gitlab-icon-1-color-white-rgb.svg'


const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class NavAppBar extends Component {
  render() {
    const { classes } = this.props;
    return (
      <>
        <AppBar>
          <Grid>
            <Row>

              <Col sm={6}>
              <Toolbar>
                <a href="/card"> <Typography variant="h6" color="white" className={classes.grow}>Card</Typography></a>

              </Toolbar>
            </Col>
              {/*<Col sm={4}>*/}
                {/*<Toolbar>*/}
                  {/*<a href="/graph"> <Typography variant="h6" color="white" className={classes.grow}>Graph</Typography></a>*/}

                {/*</Toolbar>*/}
              {/*</Col>*/}

              <Col sm={6}>
                <Toolbar>
                  <Typography variant="h6" color="inherit" className={classes.grow}>
                    Parhunt
                  </Typography>
                  <Tooltip title="Ke laman Git proyek ini!">
                    <Button color="inherit" href="https://gitlab.com/chez14/parhunt/">
                      <img src={gitlab_logo} alt="Gitlab Logo" style={{height: 40, width:"auto"}}/>
                    </Button>
                  </Tooltip>
                </Toolbar>
              </Col>

            </Row>
          </Grid>
        </AppBar>
      </>
    )
  }
};

NavAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavAppBar);