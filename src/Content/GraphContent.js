import { ArcherContainer, ArcherElement } from 'react-archer';
import React,{Component} from 'react';
import {  Row, Col } from 'react-flexbox-grid'
import Graph from '../general/graph/graph'
import ReactGA from "react-ga";
import {inject, observer} from "mobx-react";
import {withStyles} from "@material-ui/core";


class GraphContent extends Component{
    componentDidMount() {
        const { appstore } = this.props;
        appstore.loadMatakuliah();

        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    render() {
        const {  appstore } = this.props;
        return(
            <Row>
                {appstore.matakuliah.length===0?'kosong':''}
                {appstore.matakuliah.map((data, i) =>
                    <Col sm={6} md={4} lg={3} key={i}>
                        <Graph data={data} />
                    </Col>
                )}
            </Row>

        );

    }


}
export default inject("appstore")(observer(GraphContent));