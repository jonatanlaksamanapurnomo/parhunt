import React ,{Component} from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid'
import Carudo from '../general/carudo/carudo'
import ReactGA from "react-ga";
import {inject, observer} from "mobx-react";
import {withStyles} from "@material-ui/core";
import grey from "@material-ui/core/colors/grey";
// import WOW from "wowjs";


const styles = {
    page_color: {
        backgroundColor: grey['200'],
        minHeight:'100vh',
        overflow: 'auto'
    },
    start_row: {
        marginTop: "2rem"
    },
    card_parent: {
        padding: '0.5rem'
    }
}

class CardContent extends Component{
    componentDidMount() {
        const { appstore } = this.props;
        appstore.loadMatakuliah();
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    render(){
        const { classes, appstore } = this.props;
        return(
            <Row className={classes.start_row}>
                {appstore.matakuliah.length===0?'kosong':''}
                {appstore.matakuliah.map((data, i) =>
                    <Col sm={6} md={4} lg={3} className={classes.card_parent } key={i}>
                        <Carudo data={data} />
                    </Col>
                )}
            </Row>
        );
    }


}
export default inject("appstore")(withStyles(styles)(observer(CardContent)));