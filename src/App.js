import React, { Component } from 'react';
import NavAppBar from './general/navappbar';
import Index from './index/index';

import { Provider } from 'mobx-react';
import AppStore from './store/appstore';
import {BrowserRouter} from 'react-router-dom'


class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Provider appstore={new AppStore()}>
                <div style={{marginTop:64}}>
                    <NavAppBar />
                    <Index />

                </div>
            </Provider>
        </BrowserRouter>

    );
  }
}

export default App;
